# php-server #

### Accept POST requests and queue in Redis ###

* This is a micro-server for doing some basic error parsing and logging.

### Quick setup ###

* Install php5-[fpm,cli] depending on how you want to the server. And redis.
* On debian-based systems:
```
sudo apt-get install php5-cli php5-fpm php5-dev redis-server -y
```

* Install phpredis:
```
git clone https://github.com/phpredis/phpredis.git && cd phpredis
phpize
./configure
make && make install
echo 'extension=redis.so' | tee /etc/php5/{fpm,cli}/conf.d/redis.ini > /dev/null
service php5-fpm restart
```

### Running the app ###

```
cd /tmp && git clone https://crozzy@bitbucket.org/crozzy/php-server.git && cd php-server
```
* Locally
```
php -S 127.0.0.1:8001
```

* With NGINX
```
cp server.conf /etc/nginx/sites-available/
ln -s /etc/nginx/sites-{available,enabled}/server.conf
service nginx restart
```
* With docker
```
docker pull redis
docker run --name kredis -v /path/to/host/dbstorage:/data -d redis redis-server --appendonly yes
docker build -t php-server:latest -rm php-server/
docker run --link kredis:db -p 80:80 -i -v /path/to/host/logstorage:/var/log -t php-server:latest
```

### Configuration ###

Configuration can be managed with server.ini

* QUEUE_NAME: The key name of the redis Array we will push data to.
* REDIS_HOST: Redis host, traditionally localhost, but can be external
* REDIS_PORT: Redis port, traditionally 6379.
* ERROR_LOG: File that hold the application logs