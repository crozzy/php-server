FROM ubuntu:14.04


RUN \
    apt-get update && \ 
    apt-get install php5-fpm php5-dev nginx git -y

RUN \
    sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php5/fpm/php-fpm.conf && \
    sed -i -e "s/;listen.mode = 0660/listen.mode = 0755/g" /etc/php5/fpm/pool.d/www.conf && \
    find /etc/php5/cli/conf.d/ -name "*.ini" -exec sed -i -re 's/^(\s*)#(.*)/\1;\2/g' {} \; 


RUN \
    cd /tmp && git clone https://github.com/phpredis/phpredis.git && \
    cd phpredis && \
    phpize && \
    ./configure && \
    make && \
    make install && \
    echo 'extension=redis.so' > /etc/php5/fpm/conf.d/redis.ini

ADD . /opt/php-server

RUN \
    cp /opt/php-server/server.conf /etc/nginx/sites-available/ && \
    ln -s /etc/nginx/sites-available/server.conf /etc/nginx/sites-enabled/server.conf && \
    rm /etc/nginx/sites-enabled/default && \
    echo "daemon off;" >> /etc/nginx/nginx.conf

RUN ln -sf /dev/stdout /var/log/nginx/access.log
RUN ln -sf /dev/stderr /var/log/nginx/error.log

CMD ["/bin/bash", "/opt/php-server/upstart.sh"]
