<?php

// Global
$CONF = parse_ini_file("server.ini");

// Helpers
function logEvent($message, $body, $type) {
    $error_log = $GLOBALS['CONF']['ERROR_LOG'];
    $error_data = Array('['.date(DATE_ATOM).']', $type, $message, $body, $_SERVER['REMOTE_ADDR']);
    $error_line = join(' - ', $error_data)."\n";
    error_log($error_line, 3, $error_log);
};

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $rest_json = file_get_contents("php://input");
    if (!json_decode($rest_json)) {
        $message = array(
            'error' => 'invalid JSON'
        );
        logEvent($message['error'], $rest_json, 'ERROR');
        http_response_code(400);
        exit(json_encode($message));
    }
    header('Content-type: application/json');
    logEvent('Received POST request', $rest_json, 'INFO');

    // One connection per request
    $db_host = $CONF['REDIS_HOST'];
    if (!$db_host) {
        $db_host = '127.0.0.1';
    };

    $redis = new Redis();
    $connected = $redis->connect($db_host, $CONF['REDIS_PORT']);
    if (!$connected) {
        $message = array(
            'error' => 'could not connect to queue'
        );
        logEvent($message['error'], $rest_json, 'ERROR');
        http_response_code(503);
        exit(json_encode($message));
    };

    $success = $redis->lPush($CONF['QUEUE_NAME'], $rest_json);
    if (!$success) {
        $message = array(
            'error' => 'could not write to queue'
        );
        logEvent($message['error'], $rest_json, 'ERROR');
        http_response_code(503);
        exit(json_encode($message));
    };
    $message = array(
        'success' => 'data is queued'
    );

} else {

    http_response_code(405);
    $message = array(
        'error' => 'please use POST verb'
    );
    logEvent($message['error'], $rest_json, 'ERROR');
}
exit(json_encode($message));
?>
