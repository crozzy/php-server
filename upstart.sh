#!/bin/bash

# This is a hack because the shipped version of php-fpm on ubuntu 14:04 will strip out
# the much needed env vars and not leave an `clear_env` setting.
# In docker linking containers gives you certain env vars to communicate
echo "env[DB_PORT_6379_TCP_ADDR] = $DB_PORT_6379_TCP_ADDR" >> /etc/php5/fpm/pool.d/www.conf

service php5-fpm restart
/usr/sbin/nginx
